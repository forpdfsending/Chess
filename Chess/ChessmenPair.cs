﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    internal class ChessmenPair
    {
        public int x1;
        public int y1;

        public int x2;
        public int y2;
    }
}

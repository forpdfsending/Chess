using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;

namespace Chess
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int nRazmerDoskiGlobal;

        private void Form1_Load(object sender, EventArgs e)
        {
            //  draw();
        }

        private void draw1(int rowCount = 24, int columnCount = 24, int lCount = 0)
        {
            int sqaureWidth = 25;
            int sqaureHeight = 25;
            int ellipseWidth = 15;
            int ellipseHeight = 15;

            Bitmap bm = new Bitmap(sqaureWidth * columnCount, sqaureHeight * rowCount);
            using (Graphics g = Graphics.FromImage(bm))
            {
                using (Graphics g2 = Graphics.FromImage(bm))
                {
                    for (int i = 0; i < columnCount; i++)
                    {
                        for (int j = 0; j < rowCount; j++)
                        {
                            if ((j % 2 == 0 && i % 2 == 0) || (j % 2 != 0 && i % 2 != 0))
                            {
                                g.FillRectangle(Brushes.White, i * sqaureWidth, j * sqaureHeight, sqaureWidth, sqaureHeight);
                                g.FillEllipse(Brushes.Red, i * sqaureWidth + 5, j * sqaureHeight + 5, ellipseWidth, ellipseHeight);
                            }
                            else if ((j % 2 == 0 && i % 2 != 0) || (j % 2 != 0 && i % 2 == 0))
                            {
                                g.FillRectangle(Brushes.Black, i * sqaureWidth, j * sqaureHeight, sqaureWidth, sqaureHeight);
                                g.FillEllipse(Brushes.Red, i * sqaureWidth + 5, j * sqaureHeight + 5, ellipseWidth, ellipseHeight);
                            }
                        }
                    }
                }
            }
            // BackgroundImage = bm;


            pictureBox1.Image = bm;
        }

        //List<Chessman> positions = new();
        int[,] arr;
        List<ChessmenPair> foundPositions;

        private void clearMainArr()
        {
            for (int x = 0; x < arr.GetLength(0); x++)
            {
                for (int y = 0; y < arr.GetLength(1); y++)
                {
                    arr[x, y] = 0; // 0 - Nothing at this pos
                }
            }
        }

        private void clearFoundArr()
        {
            foundPositions = new List<ChessmenPair>();
        }

        //private int getXPositionByArrSize(int x)
        //{
        //    int xLimit = arr.GetLength(0) - 1;

        //    if (x > 0)
        //    {
        //        return x < xLimit ? x : xLimit;
        //    }
        //    else
        //    {
        //        return x < 0 ? 0 : x;
        //    }
        //}

        //private int getYPositionByArrSize(int y)
        //{
        //    int yLimit = arr.GetLength(1) - 1;

        //    if (y > 0)
        //    {
        //        return y < yLimit ? y : yLimit;
        //    }
        //    else
        //    {
        //        return y < 0 ? 0 : y;
        //    }
        //}

        private void hit(int x, int y)
        {
            if ((x + 1) < nRazmerDoskiGlobal)
            {
                arr[(x + 1), y] = 8;
            }

            if ((x - 1) >= 0)
            {
                arr[(x - 1), y] = 8;
            }

            if (y - 1 >= 0)
            {
                if (x - 1 >= 0)
                {
                    arr[(x - 1), y - 1] = 8;
                }

                arr[x, y - 1] = 8;

                if (x + 1 < nRazmerDoskiGlobal)
                {
                    arr[(x + 1), y - 1] = 8;
                }
            }

            if (y + 1 < nRazmerDoskiGlobal)
            {
                if (x - 1 >= 0)
                {
                    arr[(x - 1), y + 1] = 8;
                }

                arr[x, y + 1] = 8;

                if (x + 1 < nRazmerDoskiGlobal)
                {
                    arr[(x + 1), y + 1] = 8;
                }
            }

        }

        private bool collidedWithOtherChessmenHits(int x, int y)
        {
            bool collided = false;

            if (arr[x, y] != 0)
            {
                collided = true;
            }

            if (x - 1 >= 0)
            {
                if (arr[(x - 1), y] != 0)
                {
                    collided = true;
                }
            }

            if (x + 1 < nRazmerDoskiGlobal)
            {
                if (arr[(x + 1), y] != 0)
                {
                    collided = true;
                }
            }

            if (y - 1 >= 0)
            {
                if (x - 1 >= 0)
                {
                    if (arr[(x - 1), (y - 1)] != 0)
                    {
                        collided = true;
                    }
                }

                if (arr[x, (y - 1)] != 0)
                {
                    collided = true;
                }

                if (x + 1 < nRazmerDoskiGlobal)
                {
                    if (arr[(x + 1), (y - 1)] != 0)
                    {
                        collided = true;
                    }
                }
            }

            if (y + 1 < nRazmerDoskiGlobal)
            {
                if (x - 1 >= 0)
                {
                    if (arr[(x - 1), (y + 1)] != 0)
                    {
                        collided = true;
                    }
                }

                if (arr[x, (y + 1)] != 0)
                {
                    collided = true;
                }

                if (x + 1 < nRazmerDoskiGlobal)
                {
                    if (arr[(x + 1), (y + 1)] != 0)
                    {
                        collided = true;
                    }
                }
            }

            return collided;

        }

        public bool checkFoundPos(int x1, int y1, int x2, int y2)
        {
            bool foundNew = true;

            foreach (ChessmenPair foundChessman in foundPositions)
            {
                if (
                    (foundChessman.x1 == x1 && foundChessman.y1 == y1
                    && foundChessman.x2 == x2 && foundChessman.y2 == y2)
                    ||
                    (foundChessman.x1 == x2 && foundChessman.y1 == y2
                    && foundChessman.x2 == x1 && foundChessman.y2 == y1)
                    )
                {
                    foundNew = false;
                    break;
                }
            }

            if (foundNew)
            {
                ChessmenPair chessmenPair = new ChessmenPair();
                chessmenPair.x1 = x1;
                chessmenPair.y1 = y1;
                chessmenPair.x2 = x2;
                chessmenPair.y2 = y2;
                foundPositions.Add(chessmenPair);
            }

            return foundNew;
        }

        private void calc(int nRazmerDoski, int lDobavitFigur, List<Chessman> alreadyPlacedPositionsInp)
        {
            clearFoundArr();

            List<Chessman> alreadyPlacedPositions = new List<Chessman>();
            alreadyPlacedPositions.AddRange(alreadyPlacedPositionsInp);

            // Dictionary is analogue to C++ Map
            Dictionary<string, string> resultDict = new Dictionary<string, string>();
            List<string> resultList = new List<string>();

            for (int x = 0; x < nRazmerDoski; x++)
            {
                for (int y = 0; y < nRazmerDoski; y++)
                {
                    clearMainArr();
                    alreadyPlacedPositions.Clear();
                    alreadyPlacedPositions.AddRange(alreadyPlacedPositionsInp);

                    foreach (Chessman chessman in alreadyPlacedPositions)
                    {
                        if (chessman.x < nRazmerDoskiGlobal && chessman.y < nRazmerDoskiGlobal)
                        {
                            arr[chessman.x, chessman.y] = 1; // 1 - placed chessman!
                            hit(chessman.x, chessman.y);
                        }
                    }

                    int foundCnt = 0;

                    arr[x, y] = 1; // 1 - placed chessman!
                    hit(x, y);
                    Console.WriteLine(arr);
                    foundCnt++;

                    for (int xInner = 0; xInner < nRazmerDoski; xInner++)
                    {
                        for (int yInner = 0; yInner < nRazmerDoski; yInner++)
                        {
                            if (arr[xInner, yInner] == 0
                                ) // 0 - Nothing at this pos
                                  //bool collided = collidedWithOtherChessmenHits(xInner, yInner);
                                  //Console.WriteLine(arr);
                                  //if (!collided)
                            {
                                arr[xInner, yInner] = 1; // 1 - placed chessman!
                                if (lDobavitFigur > 2)
                                {
                                    hit(xInner, yInner);
                                    Console.WriteLine(arr);
                                }
                                foundCnt++;
                            }

                            if (lDobavitFigur > 2)
                            {
                                alreadyPlacedPositions.Add(new Chessman() { x = xInner, y = yInner });
                            }
                        }
                    }

                    Console.WriteLine(arr);

                    if (foundCnt >= lDobavitFigur)
                    {
                        for (int x2 = 0; x2 < nRazmerDoski; x2++)
                        {
                            for (int y2 = 0; y2 < nRazmerDoski; y2++)
                            {
                                if (arr[x2, y2] == 1)
                                {
                                    if (checkFoundPos(x, y, x2, y2))
                                    {
                                        if (lDobavitFigur > 2)
                                        {
                                            if (resultDict.ContainsKey($"({x}, {y})"))
                                            {
                                                resultDict[$"({x}, {y})"] =
                                                resultDict[$"({x}, {y})"] + $" ({x2}, {y2})";
                                            }
                                            else
                                            {
                                                resultDict[$"({x}, {y})"] =
                                                $"({x2}, {y2})";
                                            }
                                        }
                                        else
                                        {
                                            if (x != x2 || y != y2)
                                            {
                                                resultList.Add($"({x}, {y}) ({x2}, {y2})\r\n");
                                            }
                                        }

                                    }
                                }
                            }
                        }
                        if (lDobavitFigur == 1)
                        {
                          //  resultDict[$"({x}, {y})"] = "";
                              resultList.Add($"({x}, {y})\r\n");
                        }
                    }
                }
            }

            foreach (string key in resultDict.Keys)
            {
                richTextBox1.Text += key + " " + resultDict[key] + "\r\n";
            }

            foreach (string s in resultList)
            {
                richTextBox1.Text += s;
            }

            if (resultDict.Count() == 0 && resultList.Count() == 0)
            {
                richTextBox1.Text += "no solutions";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var inputLines = richTextBox2.Text.Split("\n");
            var pars = inputLines[0].Split(' ');
            nRazmerDoskiGlobal = int.Parse(pars[0]);
            int lDobavitFigur = int.Parse(pars[1]);
            int kAlreadyPlaced = int.Parse(pars[2]);
            List<Chessman> alreadyPlacedPositions = new List<Chessman>();

            if (inputLines.Count() > 1)
            {
                int cnt = 0;
                for (int i = 1; i < inputLines.Count(); i++)
                {
                    if (cnt < kAlreadyPlaced)
                    {
                        var coords = inputLines[i].Split(' ');
                        Chessman chessman = new Chessman();
                        chessman.x = int.Parse(coords[0]);
                        chessman.y = int.Parse(coords[1]);
                        alreadyPlacedPositions.Add(chessman);
                    }
                    cnt++;
                }
            }

            arr = new int[nRazmerDoskiGlobal, nRazmerDoskiGlobal];
            foundPositions = new List<ChessmenPair>();

            richTextBox1.Text = "";

            calc(nRazmerDoskiGlobal, lDobavitFigur, alreadyPlacedPositions);
            draw1(nRazmerDoskiGlobal, nRazmerDoskiGlobal);
        }
    }
}